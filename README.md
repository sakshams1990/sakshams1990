- 👋 Hi, I’m Saksham (@sakshams1990)
- 👀 I’m interested in Data Science and making myself a knowlegeable person in the era of ML/AI
- 🌱 I’m currently learning MLOps
- 💞️ I’m looking to collaborate on building ML pipelines and creating a top notch product using ML/AI 
- 📫 You can reach out to me at https://www.linkedin.com/in/saksham-s/ or mail me at sakshams1990@gmail.com

<!---
sakshams1990/sakshams1990 is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
